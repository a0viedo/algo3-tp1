#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>
#include <utility>
#include <string.h>
#include <sys/time.h>
using namespace std; 

// Struct con toda la data de cada curso.
struct Curso {
    Curso (int c, int i, int f) : cnumber(c), inicio(i), fin(f) {}
    Curso (const Curso& otro) : cnumber(otro.cnumber), inicio(otro.inicio), fin(otro.fin) {}
    int cnumber;
    int inicio;
    int fin;
};

// Solo para mejor lectura.
typedef vector<Curso> Cursos;
typedef vector<int> Resultado;

// Más abajo está la implementación.
bool curso_sorter(Curso const& cizq, Curso const& cder);
Resultado resolver(Cursos& c);

// Funciones y varias globales para la medicion de tiempo de ejecucion
timeval start, end;
void init_time() {
    gettimeofday(&start, 0);
}
double get_time() {
    gettimeofday(&end, 0);
    return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int main(int argc, char** argv) {
	bool medirTiempo = false;
	if(argc >= 2 && strcmp(argv[1], "tiempo") == 0) {
		medirTiempo = true;
	}
	
	char test;
	cin >> test;
	while (test != '#') {
		cin.putback(test);

		int n, i, f;

		cin >> n;
		Cursos c;
		c.reserve(n);

		for(int j = 0; j < n; j++) {
			cin >> i >> f;
			Curso nuevo(j+1, i, f);
			c.push_back(nuevo);
		}

		if (medirTiempo) {
			init_time();
			for (int i = 0; i < 1000; i++) {
				resolver(c);
			}
			// tiempo en segundos
			double time = get_time();
			
			cout << fixed << "Tiempo de ejecucion: " << time << "ms";
		}
		else {
			Resultado resultado = resolver(c);

			for(int i = 0; i < resultado.size(); i++) {
				cout << resultado[i] << " ";
			}
		}

		cout << endl;
		cin >> test;
        } 

        return 0;
}

// La magia, aqui.
Resultado resolver(Cursos& c) {
    Resultado resultado;
    resultado.reserve(c.size());
    
    // Los ordeno por fecha de fin.
    stable_sort(c.begin(), c.end(), &curso_sorter);

    // Acá seleccionamos los cursos que van a ser parte de la solución.
    resultado.push_back(c[0].cnumber);
    int ultimoFin = c[0].fin;
    
    for(int i = 1; i < c.size(); i++) {
		if(c[i].inicio > ultimoFin) {
			resultado.push_back(c[i].cnumber);
			ultimoFin = c[i].fin;
		}
    }
    // Y acá devolvemos la lista de números de cursos correspondientes a la solución encontrada.
    return resultado;
}

// Un método de comparación entre cursos para que use el sort.
bool curso_sorter(Curso const& cizq, Curso const& cder) {
	return cizq.fin < cder.fin;
}