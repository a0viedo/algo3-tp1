#include <queue>
#include <algorithm>
#include <iostream>
#include <string.h>
#include <sys/time.h>
using namespace std;

// Representacion del camion 
struct Camion 
{
	Camion() : indice(0), peso(0) {}
	Camion (int i, int p) : indice(i), peso(p) {}
	Camion (const Camion& otro) : indice(otro.indice), peso(otro.peso) {}
	int indice;
	int peso;
};

// Comparadores de camiones
class MayorPeso
{
public:
	bool operator()(Camion n1, Camion n2)
	{
		if(n1.peso > n2.peso)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};

// Varios typedefs
typedef vector<Camion> Camiones;
typedef priority_queue<Camion, Camiones, MayorPeso> CamionesPorPeso;
typedef vector<int> Paquetes;

// Prototipado de funciones
Camiones resolver(Paquetes& p, int soporte);

// Funciones y varias globales para la medicion de tiempo de ejecucion
timeval start, end;
void init_time()
{
    gettimeofday(&start, 0);
}
double get_time()
{
    gettimeofday(&end, 0);
    return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int main(int argc, char** argv) 
{
	bool medirTiempo = false;
	if(argc >= 2 && strcmp(argv[1], "tiempo") == 0)
	{
		medirTiempo = true;
	}
	
	char c;
	cin >> c;
	while (c != '#')
	{
		cin.putback(c);

		int soporte;
		int n;

		cin >> soporte;
		cin >> n;
		Paquetes p(n);

		for (int i = 0; i < n; i++)
		{
			cin >> p[i];
		}
		if (medirTiempo)
		{
			init_time();
			for (int i = 0; i < 1000; i++)
			{
				resolver(p, soporte);
			}
			
			// tiempo en segundos
			double time = get_time();
			
			cout << fixed << "Tiempo de ejecucion: " << time << "ms";
		}
		else
		{
			Camiones resultado = resolver(p, soporte);
			cout << resultado.size() << " ";

			for (int i = 0; i < resultado.size(); i++)
			{
				cout << resultado[i].peso << " ";
			}
		}

		cout << endl;
		cin >> c;
        } 

        return 0;
}

Camiones resolver(Paquetes& p, int soporte)
{		
	CamionesPorPeso camiones;
		
	for(Paquetes::iterator it = p.begin(); it != p.end(); ++it)	
	{
		if (camiones.empty() || camiones.top().peso + *it > soporte)
		{
			// Creo un nuevo camion con peso igual al paquete actual (indice es el momento en que se cargo su primer paquete)
			Camion nuevo = Camion(camiones.size(), *it);

			// Lo meto en el heap
			camiones.push(nuevo);
		}
		else
		{
			// Agarro el camion con menor peso
			Camion camion(camiones.top());
			camion.peso += *it;

			// Lo saco del heap
			camiones.pop();

			// Lo meto actualizado
			camiones.push(camion);
		}
	}
	
	Camiones resultado(camiones.size());
			
	// Cargo los valores de pesos de los camiones, en orden de carga de su primer paquete
	while(!camiones.empty())
	{
		Camion top = camiones.top();
		resultado[top.indice] = top;
		camiones.pop();
	}

	return resultado;
}