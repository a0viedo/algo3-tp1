#include <cstdlib>
#include <queue>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <algorithm>



using namespace std;



typedef pair<int, pair< int, int > > sensor;
typedef pair<int, int> casillero;

timeval start, endt;

void init_time() {
	gettimeofday(&start, 0);
}

double get_time() {
	gettimeofday(&endt, 0);
	return (1000000 * (endt.tv_sec - start.tv_sec) + (endt.tv_usec - start.tv_usec)) / 1000000.0;
}


struct Museo {
    int n;
    int m;
    vector< vector<int> > Casilleros;
    vector< sensor > Sensores;
};

struct Solucion {
    int costo;
    vector< sensor > Sensores;
};


// 0 pared
// 1 casillero libre
// 2 casillero importante
Solucion s;

bool horizontal_check(Museo &m, vector< sensor > &sensores, int i, int j, bool checkType) {
    bool noRightWall = true;
    bool noLeftWall = true;

    if (checkType) {
        for (int h = 0; h < m.Sensores.size(); h++) {
            if (m.Sensores[h].second.first == i && m.Sensores[h].second.second == j) {
                return false;
            }
        }
    }

    for (int p = 1; p < m.m; p++) {
        //check row for sensors;
        if (j + p <= (m.m - 1) && m.Casilleros[i][ j + p ] != 0 && noRightWall) {
            for (int z = 0; z < sensores.size(); z++) {
                if (sensores[z].second.first == i && sensores[z].second.second == j + p) {
                    if (!checkType) {
                        return false;
                    } else if (sensores[z].first == 1 || sensores[z].first == 2) {
                        return false;
                    }

                }
            }
        } else {
            noRightWall = !(j + p <= (m.m - 1));
        }
        if (j - p >= 0 && m.Casilleros[i][j - p] != 0 && noLeftWall) {
            for (int z = 0; z < sensores.size(); z++) {
                if (sensores[z].second.first == i && sensores[z].second.second == j - p) {
                    if (!checkType) {
                        return false;
                    } else if (sensores[z].first == 1 || sensores[z].first == 2) {
                        return false;
                    }
                }
            }
        } else {
            noLeftWall = !(j - p >= 0);
        }
        if (!(noLeftWall || noRightWall)) {
            //encountered a wall in both directions, there's no point in keep looking
            break;
        }
    }
    return true;
}


bool vertical_check(Museo &m, vector< sensor > &sensores, int i, int j, bool checkType) {
    bool noTopWall = true;
    bool noBottomWall = true;

    if (checkType) {
        for (int h = 0; h < m.Sensores.size(); h++) {
            if (m.Sensores[h].second.first == i && m.Sensores[h].second.second == j) {
                return false;
            }
        }
    }

    for (int q = 0; q < m.n; q++) {
        //check column for sensors

        if (i + q <= (m.n - 1) && m.Casilleros[ i + q ][j] != 0 && noTopWall) {
            for (int z = 0; z < sensores.size(); z++) {
                if (sensores[z].second.second == j && sensores[z].second.first == i + q) {
                    if (!checkType) {
                        return false;
                    } else if (sensores[z].first == 1 || sensores[z].first == 3) {
                        return false;
                    }
                }
            }
        } else {
            noTopWall = !(i + q <= (m.n - 1));
        }

        if (i - q >= 0 && m.Casilleros[ i - q ][j] != 0 && noBottomWall) {
            for (int z = 0; z < sensores.size(); z++) {
                if (sensores[z].second.second == j && sensores[z].second.first == i - q) {
                    if (!checkType) {
                        return false;
                    } else if (sensores[z].first == 1 || sensores[z].first == 3) {
                        return false;
                    }
                }
            }
        } else {
            noBottomWall = !(i - q >= 0);
        }
        if (!(noTopWall || noBottomWall)) {
            //encountered a wall in both directions, there's no point in keep looking
            break;
        }
    }
    return true;
}

bool check(Museo &m) {
    int cant_sensores = m.Sensores.size();

    for (int x = 0; x < cant_sensores; x++) {
        int type = m.Sensores[x].first;
        int i = m.Sensores[x].second.first;
        int j = m.Sensores[x].second.second;

        vector< sensor > temp(m.Sensores);
        temp.erase(temp.begin() + x);

        if (type == 1 && !(horizontal_check(m, temp, i, j, false) && vertical_check(m, temp, i, j, false))) {
            //es cuatridimensional
            return false;
        } else if (type == 2 && !horizontal_check(m, temp, i, j, false)) {
            //es bidimensional horizontal
            return false;
        } else if (!vertical_check(m, temp, i, j, false)) {
            //es bidimensional vertical
            return false;
        }
    }

    return true;
}

int get_cost(vector< sensor > &sensores) {
    int costo = 0;
    for (int x = 0; x < sensores.size(); x++) {
        if (sensores[x].first == 1) {
            costo += 6;
        } else {
            costo += 4;
        }
    }
    return costo;
}

void guardar_solucion(vector< sensor > &sensores) {
    vector< sensor > copia_sensores(sensores);
    s.Sensores = copia_sensores;
    s.costo = get_cost(sensores);

}

void print_museo(Museo &m) {
    for (int i = 0; i < m.n; i++) {
        for (int j = 0; j < m.m; j++) {
            cout << m.Casilleros[i][j] << " ";
        }
        cout << endl;
    }
}

bool is_solved(Museo &m) {
    if (!check(m)) {
        return false;
    }
    for (int i = 0; i < m.n; i++) {
        for (int j = 0; j < m.m; j++) {
            bool hor = horizontal_check(m, m.Sensores, i, j, true);
            bool ver = vertical_check(m, m.Sensores, i, j, true);
            if (m.Casilleros[i][j] == 1) {
                if (hor && ver) {
                    return false;
                }
            }
            if (m.Casilleros[i][j] == 2) {
                if (hor || ver) {
                    return false;
                }
            }
        }
    }
    return true;
}

vector< casillero > remover_casilleros_cubiertos(Museo &m, int h, int i, int j, vector< casillero > casilleros_vacios) {
    for (int x = 0; x < casilleros_vacios.size(); x++) {
        if (casilleros_vacios[x].first == i) {
            bool wall = false;
            if (casilleros_vacios[x].second > j) {
                for (int q = casilleros_vacios[x].second; q > 0; q--) {
                    if (m.Casilleros[i][q] == 0) {
                        wall = true;
                        break;
                    }
                }
                if (wall) {
                    continue;
                }
            } else {
                for (int q = casilleros_vacios[x].second; q < m.m; q++) {

                    if (m.Casilleros[i][q] == 0) {
                        wall = true;
                        break;
                    }
                }
                if (wall) {
                    continue;
                }
            }
            casilleros_vacios.erase(casilleros_vacios.begin() + x);
        }
        if (casilleros_vacios[x].second == j) {
            bool wall = false;
            if (casilleros_vacios[x].first > i) {
                for (int q = casilleros_vacios[x].first; q > 0; q--) {
                    if (m.Casilleros[q][j] == 0) {
                        wall = true;
                        break;
                    }
                }
                if (wall) {
                    continue;
                }
            } else {
                for (int q = casilleros_vacios[x].first; q < m.n; q++) {
                    if (m.Casilleros[q][j] == 0) {
                        wall = true;
                        break;
                    }
                }
                if (wall) {
                    continue;
                }
            }
            casilleros_vacios.erase(casilleros_vacios.begin() + x);
        }
    }
    return casilleros_vacios;
}

void print_solucion(vector< sensor > &sensores) {
    for (int x = 0; x < sensores.size(); x++) {
        cout << "tipo: " << sensores[x].first << ", i: " << sensores[x].second.first << " , j:" << sensores[x].second.second << endl;
    }
}

void backtrack(Museo &m, vector< casillero > &casilleros_vacios) {
    if ( casilleros_vacios.size() == 0 && is_solved(m) ) {
        int costo = get_cost(m.Sensores);
        if (s.costo == 0 || (s.costo != 0 && costo < s.costo)) {
            guardar_solucion(m.Sensores);
        }
    }
    for (int k = 0; k < casilleros_vacios.size(); k++) {
        for (int h = 4; h > 0; h--) {
            //pruebo con los 3 tipos de sensores que tengo
            int i, j;
            i = casilleros_vacios[k].first;
            j = casilleros_vacios[k].second;
            m.Sensores.push_back(make_pair(h, make_pair(i, j)));
            int cost = get_cost(m.Sensores);
            if(s.costo != 0 && cost >= s.costo)
            {
                //segunda poda
                m.Sensores.pop_back();
                return;
            }
            if (check(m)) {
                // si cuando lo agregué no tiene problema con el resto de los sensores
                // entonces el casillero está ocupado, lo saco de la lista de pendientes,
                // y también saco los que son cubiertos por el nuevo sensor que agregué
                vector< casillero > casilleros_restantes(casilleros_vacios);
                casilleros_restantes.erase(casilleros_restantes.begin() + k);
                casilleros_restantes = remover_casilleros_cubiertos(m, h, i, j, casilleros_restantes);
                backtrack(m, casilleros_restantes);
            }
            m.Sensores.pop_back();
        }
        // también debo tener en cuenta probar dejando el casillero libre y continuar con el resto
        vector< casillero > casilleros_restantes(casilleros_vacios);
        casilleros_restantes.erase(casilleros_restantes.begin() + k);
        backtrack(m, casilleros_restantes);
        
    }
    return;
}

int main(int argc, const char* argv[]) {

    char c;
    cin >> c;
    s.costo = 0;
    Museo m;
    vector< casillero > casilleros_vacios;
    while (c != '#') {
        cin.putback(c);

        cin >> m.n;
        cin >> m.m;

        for (int i = 0; i < m.n; i++) {
            vector<int> t;
            m.Casilleros.push_back(t);
            for (int j = 0; j < m.m; j++) {
                int value;
                cin >> value;
                m.Casilleros[i].push_back(value);
                if (value == 1) {
                    casilleros_vacios.push_back(make_pair(i, j));
                }
            }
        }


        cin >> c;
        //print_museo(m);

    }
    init_time();
    double time;
    backtrack(m, casilleros_vacios);
    time = get_time();
    if(casilleros_vacios.size() != 0 && s.costo == 0)
    {
        cout << "-1" << endl;
    }
    else
    {
        cout << s.Sensores.size() << " " << s.costo << endl;

        for(int n = 0; n < s.Sensores.size(); n++)
        {
            cout << s.Sensores[n].first << " " << s.Sensores[n].second.first << " " << s.Sensores[n].second.second << endl;
        }
    }
    cout << time <<"s" << endl;
    return 0;
}